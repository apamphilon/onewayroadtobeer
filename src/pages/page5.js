import styled from 'styled-components';
import Tile from '../components/Tile';
import { JULY_19 } from '../constants/dates';

const StyledPage5 = styled.div`
  height: 100vh;
  padding-left: 48px;

  /* Grid Fallback */
  display: flex;
  flex-wrap: wrap;

  /* Supports Grid */
  display: grid;
  grid-template-rows: 1fr 1fr;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-areas:
    'one two two'
    'three four five';

  @media only screen and (max-width: 930px) {
    display: flex;
    flex-direction: column;
    flex-wrap: nowrap;
    padding-left: 0;
    padding-top: 96px;
  }
`;

function Page5() {
  return (
    <StyledPage5>
      <Tile
        title={`You can now have \nbeer in a pub, \npark, or garden, \nwith five friends!`}
        backgroundColor="#FFB612"
        gridArea="one"
        stageComplete
      />
      <Tile
        title={`Beer everywhere, \nwith everyone`}
        backgroundColor="#8965DD"
        gridArea="two"
        date={new Date(JULY_19)}
        hasCountdown
      />
      <Tile gridArea="three" allowGrow />
      <Tile backgroundColor="#FA9CBE" gridArea="four" allowGrow />
      <Tile backgroundColor="#93D1AC" gridArea="five" allowGrow />
    </StyledPage5>
  );
}

export default Page5;
